var WIDTH = 1280;
var HEIGHT = 720;
var MOUSE_X = 0;
var MOUSE_Y = 0;

async function main() {	
	const canvas = document.querySelector("canvas");
	if (!navigator.gpu) {
		throw new Error("WebGPU not supported on this browser.");
	}
	const adapter = await navigator.gpu.requestAdapter();
	if (!adapter) {
		throw new Error("No appropriate GPUAdapter found.");
	}
	const device = await adapter.requestDevice();
	const context = canvas.getContext("webgpu");
	const canvasFormat = navigator.gpu.getPreferredCanvasFormat();
	context.configure({
		device: device,
		format: canvasFormat,
	});
	
	const uniformBufferSize =
		1 * 4 + // aspectRatio is 1 32bit floats (4bytes)
		1 * 4 + // time is 1 32bit floats (4bytes)
		2 * 4 + // mousePos is 2 32bit floats (4bytes)
		2 * 4;  // screenSize is 2 32bit floats (4bytes)
	const uniformBuffer = device.createBuffer({
		size: uniformBufferSize,
		usage: GPUBufferUsage.UNIFORM | GPUBufferUsage.COPY_DST,
	});

	const uniformValues = new Float32Array(uniformBufferSize / 4);
	uniformValues.set([WIDTH / HEIGHT], 0);
	uniformValues.set([performance.now()], 1);
	uniformValues.set([MOUSE_X, MOUSE_Y], 2);
	uniformValues.set([WIDTH, HEIGHT], 4);

	const vertices = new Float32Array([
		// X, Y,
		-1, -1, // Triangle 1 (Blue)
		1, -1,
		1,  1,
		-1, -1, // Triangle 2 (Red)
		1,  1,
		-1,  1,
	]);
	const vertexBuffer = device.createBuffer({
		label: "Cell vertices",
		size: vertices.byteLength,
		usage: GPUBufferUsage.VERTEX | GPUBufferUsage.COPY_DST,
	});
	device.queue.writeBuffer(vertexBuffer, /*bufferOffset=*/0, vertices);
	const vertexBufferLayout = {
		arrayStride: 8,
		attributes: [{
			format: "float32x2",
			offset: 0,
			shaderLocation: 0, // Position, see vertex shader
		}],
	};

	const cellShaderModule = device.createShaderModule({
		label: "Cell shader",
		code: `
struct VertexOut {
	@builtin(position) position : vec4<f32>,
	@location(0) color : vec2<f32>,
};

struct Data {
	aspectRatio: f32,
	time: f32,
	mouse_x: f32,
	mouse_y: f32,
	width: f32,
	height: f32,
};

@group(0) @binding(0) var<uniform> data: Data;

@vertex
fn vertexMain(@location(0) pos: vec2f) -> VertexOut {
	var output : VertexOut;
	output.position = vec4f(pos, 0, 1);
	output.color = pos;
	output.color.x *= data.aspectRatio;
	return output;
}

@fragment
fn fragmentMain(vData: VertexOut) -> @location(0) vec4f {
	return (vec4f(palette(fractal(vData.color)), 1));
}

fn palette(t: f32) -> vec3f {
	var a:vec3f;
	var b:vec3f;
	var c:vec3f;
	var d:vec3f;

	a = vec3f(0.809, 0.040, 0.318);
	b = vec3f(0.419, 0.899, 0.574);
	c = vec3f(0.416, 0.668, 0.273);
	d = vec3f(0.372, 0.196, 1.944);

	return (a + b * cos(6.28328 * (c * t + d)));
}

fn fractal(uv: vec2f) -> f32 {
	var col: f32;
	var z_r: f32;
	var z_i: f32;
	var temp_r: f32;
	var temp_i: f32;
	var c_r: f32;
	var c_i: f32;
	var i: i32;

	z_r = uv.x / ((cos(data.time / 5000) + 2) * 6 - 5);
	z_i = uv.y / ((cos(data.time / 5000) + 2) * 6 - 5);
	c_r = 0.15 * (data.mouse_x / data.width - 0.5) + 0.4;
	c_i = 0.15 * (data.mouse_y / data.height - 0.5) + 0.35;

	while (z_r * z_r + z_i * z_i < 200 && i < 400)
	{
		temp_r = z_r * z_r - z_i * z_i + c_r;
		temp_i = z_r * z_i * 2 + c_i;
		if (z_r == temp_r && z_i == temp_i)
		{
			break ;
		}
		z_r = temp_r;
		z_i = temp_i;
		i++;
	}
	return ((f32(i) / 50) + data.time / 5000);
}
	`
	});
	const pipeline = device.createRenderPipeline({
		label: "Cell pipeline",
		layout: "auto",
		vertex: {
			module: cellShaderModule,
			entryPoint: "vertexMain",
			buffers: [vertexBufferLayout]
		},
		fragment: {
			module: cellShaderModule,
			entryPoint: "fragmentMain",
			targets: [{
				format: canvasFormat
			}]
		}
	});

	const bindGroup = device.createBindGroup({
		layout: pipeline.getBindGroupLayout(0),
		entries: [{ binding: 0, resource: { buffer: uniformBuffer }}]
	});

	function getMousePos(e) {
		return {x:e.clientX,y:e.clientY};
	}

	document.onmousemove=function(e) {
		var mousecoords = getMousePos(e);
		MOUSE_X = mousecoords.x;
		MOUSE_Y = mousecoords.y;
	};

	function render() {
		uniformValues.set([WIDTH / HEIGHT], 0);
		uniformValues.set([performance.now()], 1);
		uniformValues.set([MOUSE_X, MOUSE_Y], 2);
		uniformValues.set([WIDTH, HEIGHT], 4);
		device.queue.writeBuffer(uniformBuffer, 0, uniformValues);
		const encoder = device.createCommandEncoder();
		const pass = encoder.beginRenderPass({
			colorAttachments: [{
				view: context.getCurrentTexture().createView(),
				loadOp: "clear",
				clearValue: { r: 0, g: 0, b: 0.4, a: 1 },
				storeOp: "store",
			}]
		});
		pass.setPipeline(pipeline);
		pass.setBindGroup(0, bindGroup);	
		pass.setVertexBuffer(0, vertexBuffer);
		pass.draw(vertices.length / 2); // 6 vertices
		pass.end();
		device.queue.submit([encoder.finish()]);
		requestAnimationFrame(render);
	}

	const observer = new ResizeObserver(entries => {
		for (const entry of entries) {
			const canvas = entry.target;
			const width = entry.contentBoxSize[0].inlineSize;
			const height = entry.contentBoxSize[0].blockSize;
			WIDTH = width;
			HEIGHT = height;
			canvas.width = Math.max(1, Math.min(width, device.limits.maxTextureDimension2D));
			canvas.height = Math.max(1, Math.min(height, device.limits.maxTextureDimension2D));
			// re-render
			requestAnimationFrame(render);
		}
	});
	observer.observe(canvas);
}

main();
