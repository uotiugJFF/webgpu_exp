var WIDTH = 1280;
var HEIGHT = 720;
var MOUSE_X = 0;
var MOUSE_Y = 0;

async function main() {	
	const canvas = document.querySelector("canvas");
	if (!navigator.gpu) {
		throw new Error("WebGPU not supported on this browser.");
	}
	const adapter = await navigator.gpu.requestAdapter();
	if (!adapter) {
		throw new Error("No appropriate GPUAdapter found.");
	}
	const device = await adapter.requestDevice();
	const context = canvas.getContext("webgpu");
	const canvasFormat = navigator.gpu.getPreferredCanvasFormat();
	context.configure({
		device: device,
		format: canvasFormat,
	});
	
	const uniformBufferSize =
		1 * 4 + // aspectRatio is 1 32bit floats (4bytes)
		1 * 4 + // time is 1 32bit floats (4bytes)
		2 * 4;  // mousePos is 2 32bit floats (4bytes)
	const uniformBuffer = device.createBuffer({
		size: uniformBufferSize,
		usage: GPUBufferUsage.UNIFORM | GPUBufferUsage.COPY_DST,
	});

	const uniformValues = new Float32Array(uniformBufferSize / 4);
	uniformValues.set([WIDTH / HEIGHT], 0);
	uniformValues.set([performance.now()], 1);
	uniformValues.set([MOUSE_X, MOUSE_Y], 2);

	const vertices = new Float32Array([
		// X, Y,
		-1, -1, // Triangle 1 (Blue)
		1, -1,
		1,  1,
		-1, -1, // Triangle 2 (Red)
		1,  1,
		-1,  1,
	]);
	const vertexBuffer = device.createBuffer({
		label: "Cell vertices",
		size: vertices.byteLength,
		usage: GPUBufferUsage.VERTEX | GPUBufferUsage.COPY_DST,
	});
	device.queue.writeBuffer(vertexBuffer, /*bufferOffset=*/0, vertices);
	const vertexBufferLayout = {
		arrayStride: 8,
		attributes: [{
			format: "float32x2",
			offset: 0,
			shaderLocation: 0, // Position, see vertex shader
		}],
	};

	const cellShaderModule = device.createShaderModule({
		label: "Cell shader",
		code: `
struct VertexOut {
	@builtin(position) position : vec4<f32>,
	@location(0) color : vec2<f32>,
};

struct Data {
	aspectRatio: f32,
	time: f32,
	mouse_x: f32,
	mouse_y: f32,
};

@group(0) @binding(0) var<uniform> data: Data;

@vertex
fn vertexMain(@location(0) pos: vec2f) -> VertexOut {
	var output : VertexOut;
	output.position = vec4f(pos, 0, 1);
	output.color = pos;
	output.color.x *= data.aspectRatio;
	return output;
}

@fragment
fn fragmentMain(vData: VertexOut) -> @location(0) vec4f {
	var ro : vec3f;
	var rd : vec3f;
	var	t : f32;
	var p : vec3f;
	var d : f32;
	var i : u32;
	var j : f32;
	var col : vec3f;
	ro = vec3f(0, 0, -3);
	rd = normalize(vec3f(vData.color * (sin(data.time / 1000.0) + 1.5) / 4, 1)); // 0.8 change the fov
	t = 0.0;
	for (i = 0; i < 80; i++)
	{
		p = ro + (rd * t);
	//	p.y += sin(t) * 0.3;
		p = rotZ(p, t * 0.08);
		p = rotY(p, t * 0.06);
		p = rotX(p, t * 0.04);
		d = map(p);
		t += d;
		if (d < 0.001 || t > 100.0) { break; }
	}
	j = f32(i);
	col = palette(t * 0.05 + j * 0.005);
	return vec4f(col, 1);
}

fn map(p: vec3f) -> f32 {
	var sph: f32;
	var sphPos : vec3f;
	var box: f32;
	var ground: f32;
	var q: vec3f;

//	sphPos = vec3(sin(data.time / 1000.0) * 3, 0, 0);
//	sph = sdSphere(p - sphPos, 1);
	q = p;
	q = rotZ(q, -data.mouse_x / 200.0);
	q = rotX(q, data.mouse_y / 200.0);
	q.z += data.time / 1000.0;
	q = fract(q) - 0.5;
	box = sdBox(q * 2, vec3f(0.25)) / 2.0;
//	ground = p.y + 0.75;
//	return (smin(ground, smin(sph, box, 2), 1));
	return (box);
}

fn rotX(vec: vec3f, angle: f32) -> vec3f {
	return (vec3f(vec.x, vec.y * cos(angle) + vec.z * sin(angle), 
		-vec.y * sin(angle) + vec.z * cos(angle)));
}

fn rotY(vec: vec3f, angle: f32) -> vec3f {
	return (vec3f(vec.x * cos(angle) - vec.z * sin(angle), vec.y, 
		vec.x * sin(angle) + vec.z * cos(angle)));
}

fn rotZ(vec: vec3f, angle: f32) -> vec3f {
	return (vec3f(vec.x * cos(angle) + vec.y * sin(angle), 
		-vec.x * sin(angle) + vec.y * cos(angle), vec.z));
}

fn sdBox(p: vec3f, b: vec3f) -> f32 {
	var q: vec3f;
	var vecMax: vec3f;

	q = abs(p) - b;
	vecMax = vec3f(max(q.x, 0), max(q.y, 0), max(q.z, 0));
	return (length(vecMax) + min(max(q.x, max(q.y, q.z)), 0));
}

fn sdSphere(p: vec3f, s: f32) -> f32 {
	return (length(p) - s);
}

fn smin(a: f32, b: f32, k:f32) -> f32 {
	var h: f32;

	h = max(k - abs(a - b), 0) / k;
	return (min(a, b) - h * h * h * k * (1.0 / 6.0));
}

fn palette(t: f32) -> vec3f {
	var a:vec3f;
	var b:vec3f;
	var c:vec3f;
	var d:vec3f;

	a = vec3f(1.0, 0.5, 0.3);
	b = vec3f(-0.3, 0.6, 0.2);
	c = vec3f(1.1, 1.0, 1.0);
	d = vec3f(-1.1, 0.6, 1.0);

	return (a + b * cos(6.28328 * (c * t + d)));
}
	`
	});
	const pipeline = device.createRenderPipeline({
		label: "Cell pipeline",
		layout: "auto",
		vertex: {
			module: cellShaderModule,
			entryPoint: "vertexMain",
			buffers: [vertexBufferLayout]
		},
		fragment: {
			module: cellShaderModule,
			entryPoint: "fragmentMain",
			targets: [{
				format: canvasFormat
			}]
		}
	});

	const bindGroup = device.createBindGroup({
		layout: pipeline.getBindGroupLayout(0),
		entries: [{ binding: 0, resource: { buffer: uniformBuffer }}]
	});

	function getMousePos(e) {
		return {x:e.clientX,y:e.clientY};
	}

	document.onmousemove=function(e) {
		var mousecoords = getMousePos(e);
		MOUSE_X = mousecoords.x;
		MOUSE_Y = mousecoords.y;
	};

	function render() {
		uniformValues.set([WIDTH / HEIGHT], 0);
		uniformValues.set([performance.now()], 1);
		uniformValues.set([MOUSE_X, MOUSE_Y], 2);
		device.queue.writeBuffer(uniformBuffer, 0, uniformValues);
		const encoder = device.createCommandEncoder();
		const pass = encoder.beginRenderPass({
			colorAttachments: [{
				view: context.getCurrentTexture().createView(),
				loadOp: "clear",
				clearValue: { r: 0, g: 0, b: 0.4, a: 1 },
				storeOp: "store",
			}]
		});
		pass.setPipeline(pipeline);
		pass.setBindGroup(0, bindGroup);	
		pass.setVertexBuffer(0, vertexBuffer);
		pass.draw(vertices.length / 2); // 6 vertices
		pass.end();
		device.queue.submit([encoder.finish()]);
		requestAnimationFrame(render);
	}

	const observer = new ResizeObserver(entries => {
		for (const entry of entries) {
			const canvas = entry.target;
			const width = entry.contentBoxSize[0].inlineSize;
			const height = entry.contentBoxSize[0].blockSize;
			WIDTH = width;
			HEIGHT = height;
			canvas.width = Math.max(1, Math.min(width, device.limits.maxTextureDimension2D));
			canvas.height = Math.max(1, Math.min(height, device.limits.maxTextureDimension2D));
			// re-render
			requestAnimationFrame(render);
		}
	});
	observer.observe(canvas);
}

main();
